package cn.itcast.dtx.tccdemo.bank1.service.impl;

import cn.itcast.dtx.tccdemo.bank1.dao.AccountInfoDao;
import cn.itcast.dtx.tccdemo.bank1.service.AccountInfoService;
import cn.itcast.dtx.tccdemo.bank1.spring.Bank2Client;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("Bank1AccountInfoServiceImpl")
@Slf4j
public class AccountInfoServiceImpl implements AccountInfoService {

    @Autowired
    AccountInfoDao accountInfoDao;

    @Autowired
    Bank2Client bank2Client;

    /**
     * 账户扣款，就是tcc的try方法
     * @param accountNo
     * @param amount
     */
    @Override
    //只要记住@Hmily标记的方法就是try方法，在注解中指定confirm、cancel两个方法的名字
    @Hmily(confirmMethod = "commit",cancelMethod = "rollback")
    @Transactional
    public void updateAccountBalance(String accountNo, Double amount) {
        //获取全局事务id
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("try 开始执行 xid:{}",transId);
        //try幂等校验,判断local_try_log表中是否有try日志记录，如果有则不在执行
        if (accountInfoDao.isExistTry(transId)>0) {
            log.info("bank2 try 已经执行，无需重复执行，xid:{}",transId);
            return;
        }
        //try悬挂处理,如果cancel、confirm有一个已经执行了，try不在执行
        if (accountInfoDao.isExistConfirm(transId)>0||accountInfoDao.isExistCancel(transId)>0){
            log.info("bank2 悬挂处理，confirm或cancel 已经执行，无需重复执行，xid:{}",transId);
            return;
        }
        //扣减金额
        if (accountInfoDao.subtractAccountBalance(accountNo, amount)<=0) {
            //扣减失败
            throw new RuntimeException("bank1 try 扣减金额失败，xid："+transId);
        }
        //插入try执行记录
        accountInfoDao.addTry(transId);
        //远程调用李四，转账
        if (!bank2Client.transfer(-amount)) {
            throw new RuntimeException("bank1远程调用bank2 扣除金额失败，xid:"+transId);
        }
    }

    //confirm方法
    public void commit(String accountNo, Double amount){
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("confirm 开始执行 xid:{}",transId);
    }

    //cancel方法
    @Transactional
    public void rollback(String accountNo, Double amount){
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("cancel 开始执行 xid:{}",transId);
        //幂等校验
        if (accountInfoDao.isExistCancel(transId)>0) {
            log.info("bank1 cancel 已经执行，无需重复执行，xid:{}",transId);
            return;
        }
        //空回滚处理，如果try没有执行，cancel不允许执行
        if (accountInfoDao.isExistTry(transId)>0) {
            log.info("bank1 空回滚处理 try没有执行，cancel不允许执行，xid:{}",transId);
            return;
        }
        //增加可以余额
        accountInfoDao.addAccountBalance(accountNo,amount);
        //插入一条cancel的执行记录
        accountInfoDao.addCancel(transId);
    }
}
