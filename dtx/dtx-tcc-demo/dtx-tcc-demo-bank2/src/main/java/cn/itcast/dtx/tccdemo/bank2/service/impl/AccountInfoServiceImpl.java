package cn.itcast.dtx.tccdemo.bank2.service.impl;

import cn.itcast.dtx.tccdemo.bank2.dao.AccountInfoDao;
import cn.itcast.dtx.tccdemo.bank2.service.AccountInfoService;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("Bank2AccountInfoServiceImpl")
@Slf4j
public class AccountInfoServiceImpl implements AccountInfoService {

    @Autowired
    AccountInfoDao accountInfoDao;

    /**
     * 账户扣款，就是tcc的try方法
     * @param accountNo
     * @param amount
     */
    @Override
    //只要记住@Hmily标记的方法就是try方法，在注解中指定confirm、cancel两个方法的名字
    @Hmily(confirmMethod = "commit",cancelMethod = "rollback")
    @Transactional
    public void updateAccountBalance(String accountNo, Double amount) {
        //获取全局事务id
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("try 开始执行 xid:{}",transId);
    }

    //confirm方法
    public void commit(String accountNo, Double amount){
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("confirm 开始执行 xid:{}",transId);
        //幂等校验
        if (accountInfoDao.isExistConfirm(transId)>0) {
            log.info("bank2 confirm 已经执行，无需重复执行，xid:{}",transId);
            return;
        }
        accountInfoDao.addAccountBalance(accountNo,amount);
        //增加一条confirm日志
        accountInfoDao.addConfirm(transId);
    }

    //cancel方法
    @Transactional
    public void rollback(String accountNo, Double amount){
        String transId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("cancel 开始执行 xid:{}",transId);
        //幂等校验
        if (accountInfoDao.isExistCancel(transId)>0) {
            log.info("bank2 cancel 已经执行，无需重复执行，xid:{}",transId);
            return;
        }
        //空回滚处理，如果try没有执行，cancel不允许执行
        if (accountInfoDao.isExistTry(transId)>0) {
            log.info("bank2 空回滚处理 try没有执行，cancel不允许执行，xid:{}",transId);
            return;
        }
        //增加可以余额
        accountInfoDao.addAccountBalance(accountNo,amount);
        //插入一条cancel的执行记录
        accountInfoDao.addCancel(transId);
    }
}
