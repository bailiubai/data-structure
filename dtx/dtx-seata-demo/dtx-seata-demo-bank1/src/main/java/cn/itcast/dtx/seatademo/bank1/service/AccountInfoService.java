package cn.itcast.dtx.seatademo.bank1.service;

public interface AccountInfoService {

    /**
     * 扣减金额
     * @param accountNo
     * @param amount
     */
    public void updateAccountBalance(String accountNo,Double amount);

}
